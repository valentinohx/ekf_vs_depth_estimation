# EKF_VS_depth_estimation

This is the project on the purpose of fast visual servoing.

Inside the project, it includes several parts:

1. The feature detection and  tracking
2. Visual servoing 
3. Feature depth estimation

The ellipse tracking part is taken from: https://github.com/h3ct0r/fast_ellipse_detector. I would like to thank him

The project runs under ROS kinetic and GAZEBO 9, VISP is required to plot some figures,  stack-of-tasks/pinocchio
is used to handle robot dynamics and kinematics. 

The robot model used in this project are the kuka lwr4p robot; one needs to attach a gazebo camera on the tip of the arm. 
One can find a similar model over:
https://github.com/bulletphysics/bullet3/tree/master/data/kuka_lwr
however, some modifications are needed since the camera is not attached yet in the model above.

To perform the visual servoing task, a controller is needed to control the velocity of the robot joints, unfortunately it can not be provided here. 
One can use the controller provided by ROS joint_group_velocity_controller as an alternative.